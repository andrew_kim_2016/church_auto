require 'test_helper'

class WorshipSlideOpControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worship_slide = worship_slides(:one)
   
  end
  
  test "should get create" do
    get worship_slide_op_download_url params: {id: @worship_slide}
    assert_response :success
  end

  test "should get destroy" do
    get worship_slide_op_destroy_data_url  params: {id: @worship_slide}
      assert_redirected_to worship_slide_path(assigns(:worship_slide)) 
  end

end
