require 'test_helper'

class SlideversControllerTest < ActionController::TestCase
  setup do
    @slidever = slidevers(:one)
    @user = users(:michael)
    log_in_as(@user)
  end

  test "should get index" do
    get 'index'
    assert_response :success
    assert_not_nil assigns(:slidevers)
  end

  test "should get new" do
    get 'new'
    assert_response :success
  end

  test "should create slidever" do
    assert_difference('Slidever.count') do
      post :create, params: { slidever: { date: @slidever.date, verslist: @slidever.verslist } }
    end

    assert_redirected_to slidever_path(assigns(:slidever))
  end

  test "should show slidever" do
    get 'show', params: { id: @slidever }
    assert_response :success
  end

  test "should get edit" do
    get 'edit',params: { id: @slidever }
    assert_response :success
  end

  test "should update slidever" do
    patch :update, params: {id: @slidever, slidever: { date: @slidever.date, verslist: @slidever.verslist } }
    assert_redirected_to slidever_path(assigns(:slidever))
  end

  test "should destroy slidever" do
    assert_difference('Slidever.count', -1) do
      delete  :destroy, params:{id: @slidever}
    end

    assert_redirected_to slidevers_path
  end
end
