require 'test_helper'

class WorshipSlidesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worship_slide = worship_slides(:one)
    @user = users(:michael)
    log_in_as(@user)
  end

  test "should get index" do
    get worship_slides_url
    assert_response :success
  end

  test "should get new" do
    get new_worship_slide_url
    assert_response :success
  end

  test "should create worship_slide" do
    assert_difference('WorshipSlide.count') do
      post worship_slides_url, params: { worship_slide: { date: @worship_slide.date, hymn1: @worship_slide.hymn1, hymn2: @worship_slide.hymn2, hymn3: @worship_slide.hymn3, song1: @worship_slide.song1, song2: @worship_slide.song2, song3: @worship_slide.song3, song4: @worship_slide.song4, song5: @worship_slide.song5, song6: @worship_slide.song6, verse: @worship_slide.verse } }
    end

    assert_redirected_to worship_slide_url(WorshipSlide.last)
  end

  test "should show worship_slide" do
    get worship_slide_url(@worship_slide)
    assert_response :success
  end

  test "should get edit" do
    get edit_worship_slide_url(@worship_slide)
    assert_response :success
  end

  test "should update worship_slide" do
    patch worship_slide_url(@worship_slide), params: { worship_slide: { date: @worship_slide.date, hymn1: @worship_slide.hymn1, hymn2: @worship_slide.hymn2, hymn3: @worship_slide.hymn3, song1: @worship_slide.song1, song2: @worship_slide.song2, song3: @worship_slide.song3, song4: @worship_slide.song4, song5: @worship_slide.song5, song6: @worship_slide.song6, verse: @worship_slide.verse } }
    assert_redirected_to worship_slide_url(@worship_slide)
  end

  test "should destroy worship_slide" do
    assert_difference('WorshipSlide.count', -1) do
      delete worship_slide_url(@worship_slide)
    end

    assert_redirected_to worship_slides_url
  end
end
