require 'test_helper'

class FilemakersControllerTest < ActionController::TestCase
  setup do
    @filemaker = filemakers(:one)
  end

  test "should get index" do
    get 'index'
    assert_response :success
    assert_not_nil assigns(:filemakers)
  end

  test "should get new" do
    get 'new'
    assert_response :success
  end

  test "should create filemaker" do
    assert_difference('Filemaker.count') do
      post :create, params: { filemaker: { listverse: @filemaker.listverse, slidever_id: @filemaker.slidever_id }}
    end

    assert_redirected_to filemaker_path(assigns(:filemaker))
  end

  test "should show filemaker" do
    get 'show', params: {id: @filemaker}
    assert_response :success
  end

  test "should get edit" do
    get 'edit', params: {id: @filemaker}
    assert_response :success
  end

  test "should update filemaker" do
    patch :update, params: {id: @filemaker, filemaker: { listverse: @filemaker.listverse, slidever_id: @filemaker.slidever_id }}
    assert_redirected_to filemaker_path(assigns(:filemaker))
  end

  test "should destroy filemaker" do
    
    assert_difference('Filemaker.count', -1) do
      delete :destroy, params: {id: @filemaker}
    end

    assert_redirected_to filemakers_path
  end
end
