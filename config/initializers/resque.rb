# config/initializers/resque.rb
rails_root = ENV['RAILS_ROOT'] || File.dirname(__FILE__) + '/../..'
rails_env = ENV['RAILS_ENV'] || 'development'

resque_config = YAML.load_file(rails_root + '/config/resque.yml')
Resque.redis = ENV['REDIS_URL'] || resque_config[rails_env]

#Resque.redis = ENV['REDIS_URL'] # default localhost:6379
Resque::Plugins::Status::Hash.expire_in = (24 * 60 * 60) # 24hrs in seconds
Resque.logger = Logger.new(Rails.root.join('log', "#{Rails.env}_resque.log"))
