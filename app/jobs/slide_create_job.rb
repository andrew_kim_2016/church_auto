require 'active_support/dependencies'
require 'rubygems'
require 'nokogiri'         
require 'open-uri'   
require 'zip'
require 'resque'

class SlideCreateJob < ApplicationJob
  include ActiveJobStatus::Hooks
  queue_as :default

  def perform(args)
    # Do something later
    pkg = PPTX::OPC::Package.new
    baseurl = "https://www.bibleonline.ru/search/?s="
    endurl = ""
    font_size = 40

    slidever_current = Slidever.find(args[:id])

     
    listverse = slidever_current.verslist
   
    lists = listverse.split(",")
    
    lists.each do |list| 
      urlsource = baseurl+URI.encode(list," ,:")+endurl
#      puts urlsource
      page = Nokogiri::HTML(open(urlsource))
      #if page.empty? then next
      #else   
      header = page.css("h2.sprite")[0].text
      #div.version-RUSV.result-text-style-normal.text-html
      slidetext = page.css("div.biblecont ol")
      if slidetext.empty? 
        then 
          header = list
          slidetext = list
          
      end
      
      #slidetext = page.css("p.verse")[0].text
      #removing leading \n
      slidetext=slidetext.to_s
      slidetext.gsub!('<ol>',"")
      slidetext.gsub!('</ol>',"")
      slidetext.gsub!('</li>',"")
      slidetext.gsub!(/[<]\w*\s\w*\W*\w*\W\s\w*\W+/,"")
      slidetext.gsub!(/\W\s\w*\W+\w\W[>]/," ")
      slidetext.gsub!('<i>'," ")
      slidetext.gsub!('</i>'," ")
      slidetext=header+" "+slidetext

      #removing RUSV string
#      slidetext = slidetext.sub "Russian Synodal Version (RUSV)", "  "
      #adding \\n
#      formatedslidetext = slidetext.gsub!(/(\B\s(\d+))/,'\n\1')
      #replacing \\n with \n
#      formatedslidetext.gsub!("\\n","\n")
      
      slide = PPTX::Slide.new(pkg)
      
      if slidetext.length < 373 then
      font_size = 40
      elsif slidetext.length <766 then
      font_size = 28
      else
        font_size = 18
      end 
      
        
      #slide.add_textbox PPTX::cm(2, 1, 22, 2), 'Title :)', sz: 45*PPTX::POINT
      slide.add_textbox PPTX::cm(0, 0, 28, 17.5), slidetext, sz: font_size*PPTX::POINT

      pkg.presentation.add_slide(slide)
    end 
     #puts "Created file: "+'Verses '+Time.now.utc.to_s+'.pptx'
    slidever_current.data = pkg.to_zip
    slidever_current.save
#    File.open(path+filename, 'wb') {|f| f.write(pkg.to_zip) }
#      zipfile.get_output_stream("Verses.pptx") {|f| f.write(pkg.to_zip) }
   #send_data  pkg.to_zip,  :type => "application/pptx; charset=UTF-8;", :disposition =>  "attachment; filename=vrs.pptx"
    #end 

  end
end
