require 'rubygems'
require 'nokogiri'         
require 'open-uri'      
class FilemakersController < ApplicationController
  before_action :set_filemaker, only: [ :show, :edit, :update, :destroy]

  # GET /filemakers
  # GET /filemakers.json
  def index
    @filemakers = Filemaker.all
  end

  # GET /filemakers/1
  # GET /filemakers/1.json
  def show
  end

  # GET /filemakers/new
  def new
#    puts "BBB"
#    puts "AAA "+ params[:slidever].to_s
    if params[:slide] = "" then
      @filemaker = Filemaker.new
    else
      @slidever = Slidever.find(params[:slide])
      @filemaker = Filemaker.new
      @filemaker.slidever_id =@slidever.id
      @filemaker.listverse = @slidever.verslist
    end
  end

  # GET /filemakers/1/edit
  def edit
  end

  # POST /filemakers
  # POST /filemakers.json
  def create
  #puts "AAA"
   
   @filemaker = Filemaker.new(filemaker_params)

   #puts filemaker_params
   
   #fcreate(@filemaker.listverse)
  
  
  
  
    respond_to do |format|
      if @filemaker.save
        format.html { redirect_to @filemaker, notice: 'Filemaker was successfully created.' }
        format.json { render :show, status: :created, location: @filemaker }
      else
        format.html { render :new }
        format.json { render json: @filemaker.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /filemakers/1
  # PATCH/PUT /filemakers/1.json
  def update
    respond_to do |format|
      if @filemaker.update(filemaker_params)
        format.html { redirect_to @filemaker, notice: 'Filemaker was successfully updated.' }
        format.json { render :show, status: :ok, location: @filemaker }
      else
        format.html { render :edit }
        format.json { render json: @filemaker.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /filemakers/1
  # DELETE /filemakers/1.json
  def destroy
    @filemaker.destroy
    respond_to do |format|
      format.html { redirect_to filemakers_url, notice: 'Filemaker was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_filemaker
      
      @filemaker = Filemaker.find(params[:id])
  
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def filemaker_params
      params.require(:filemaker).permit(:listverse, :slidever_id)
    end
    
    def fcreate(listverse)
    pkg = PPTX::OPC::Package.new
    baseurl = "https://www.biblegateway.com/passage/?search="
    endurl = "&version=RUSV"
    
    lists = listverse.split(",")
    
    lists.each do |list| 
      urlsource = baseurl+URI.encode(list," ,:")+endurl
      page = Nokogiri::HTML(open(urlsource))   
      #div.version-RUSV.result-text-style-normal.text-html
      slidetext = page.css("div.version-RUSV.result-text-style-normal.text-html")[0].text
      #slidetext = page.css("p.verse")[0].text
      #removing leading \n
      slidetext.sub!"\n",""
      #removing RUSV string
      slidetext = slidetext.sub "Russian Synodal Version (RUSV)", "  "
      #adding \\n
      formatedslidetext = slidetext.gsub!(/(\B\s(\d+))/,'\n\1')
      #replacing \\n with \n
      formatedslidetext.gsub!("\\n","\n")
      
      slide = PPTX::Slide.new(pkg)
    
      #slide.add_textbox PPTX::cm(2, 1, 22, 2), 'Title :)', sz: 45*PPTX::POINT
      slide.add_textbox PPTX::cm(0, 0, 28, 17.5), formatedslidetext
      pkg.presentation.add_slide(slide)
  
    
    
    end 
  #  commented for production 
  #  File.open('church_auto/slides/'+'Verses '+Time.now.utc.to_s+'.pptx', 'wb') {|f| f.write(pkg.to_zip) }
    File.open('church_auto/slides/'+'Verses '+'.pptx', 'wb') {|f| f.write(pkg.to_zip) }
  
    puts "Created file: "+'Verses '+Time.now.utc.to_s+'.pptx'
  
    end
end
