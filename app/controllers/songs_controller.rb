class SongsController < ApplicationController
  before_action :set_song, only: [:show, :edit, :update, :destroy, :upload, :download, :delete_data]
  before_action :song_name_hymn, only: [:create, :update]
  # GET /songs
  # GET /songs.json
  def index
    @songs = Song.all.order(date: :desc)
  end

  # GET /songs/1
  # GET /songs/1.json
  def show
  end

  # GET /songs/new
  def new
    @song = Song.new
    @song.date = Date.today
  end

  # GET /songs/1/edit
  def edit
  end

  # POST /songs
  # POST /songs.json
  def create
    
    upload
    
    @song = Song.new(song_params)
    
    
    respond_to do |format|
      if @song.save
        format.html { redirect_to @song, notice: 'Song was successfully created.' }
        format.json { render :show, status: :created, location: @song }
      else
        format.html { render :new }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /songs/1
  # PATCH/PUT /songs/1.json
  def update
    upload
    respond_to do |format|
      if @song.update(song_params)
        format.html { redirect_to @song, notice: 'Song was successfully updated.' }
        format.json { render :show, status: :ok, location: @song }
      else
        format.html { render :edit }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /songs/1
  # DELETE /songs/1.json
  def destroy
    @song.destroy
    respond_to do |format|
      format.html { redirect_to songs_url, notice: 'Song was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  
  def upload
   uploaded_io = params[:song][:data]
   if uploaded_io != nil
      params[:song][:data] = uploaded_io.read
   end
#    File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
#     file.write(uploaded_io.read)
#    end
  end
  
  def download
    
    if @song.data == nil 
        redirect_back(fallback_location: root_path)
       # send_file path+filename, :type => 'application/pptx', :disposition => 'attachment'
    else
        send_data @song.data, filename: @song.name + ".pptx",type: 'application/pptx', disposition: 'attachment'
    end
      
  end
  
  def delete_data
    @song.data=nil
    @song.save
    respond_to do |format|
      format.html { redirect_to @song, notice: 'File data was successfully destroyed.' }
      format.json { render :show, status: :ok, location: @song }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_song
      @song = Song.find(params[:id])
    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def song_params
      params.require(:song).permit(:date, :name, :typehymn, :data)
    end
    
    # Song name change for hymns
    def song_name_hymn
      
      if params[:song][:typehymn] == "1" then
        params[:song][:name] = "[Гимн] " + params[:song][:name].to_s
      else
        params[:song][:name] =  params[:song][:name].gsub("[Гимн] ","")
       
      end
     
    end
end
