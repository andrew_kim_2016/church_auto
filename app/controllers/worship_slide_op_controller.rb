require 'zip'
class WorshipSlideOpController < ApplicationController
  before_action :set_worship_slide, only: [:create, :download, :destroy_data]
  def download
     filename = @worship_slide.date.to_formatted_s(:number)+'.pptx'
      if @worship_slide.data == nil and  Resque::Plugins::Status::Hash.get($worship_job_id) == nil
        create
#        $worship_job_id = Verseget.create(id: slidever.id)
        redirect_back(fallback_location: root_path)
       # send_file path+filename, :type => 'application/pptx', :disposition => 'attachment'
      else
        $worship_job_id = ""
        send_data @worship_slide.data, filename: filename,type: 'application/pptx', disposition: 'attachment'
      end
       puts "End worship slide download\n"
  end


def extract_slide(file_data, destination,position = 0)
  if file_data == nil 
    return 0
  else 
    if  file_data.data == nil 
      return 0
    end
  end
  
  file = Tempfile.new
  IO.binwrite(file, file_data.data)
  n = 1
  Zip::File.open(file) do |zip_file|
    
    zp =  zip_file.find_entry('ppt/slides/slide'+n.to_s+'.xml')
    
    while zp != nil
      fpath = File.join(destination, zp.name.gsub(/\d+/,(position+n).to_s))
      zip_file.extract(zp, fpath)
      n = n + 1
      puts n
      zp =  zip_file.find_entry('ppt/slides/slide'+n.to_s+'.xml')
    end
  end
  file.unlink
  black_file = "slide"+(position+n).to_s+".xml"
  `cp -rf vendor/template/ppt/slides/slide1.xml tmp/zip/template/ppt/slides/#{black_file}`
  return n
end

  def create
    
    destination = "tmp/zip/template"
     FileUtils.mkdir_p(destination)
    `cp -rf vendor/template/* tmp/zip/*`
    slide_num = 1
    slide_num += extract_slide(@worship_slide.song1,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song2,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song3,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song4,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song5,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song6,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.song7,destination,slide_num)
    puts slide_num
    
    slide_num += extract_slide(@worship_slide.hymn1,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.verse,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.hymn2,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.hymn3,destination,slide_num)
    puts slide_num
    slide_num += extract_slide(@worship_slide.hymn4,destination,slide_num)
    puts slide_num
    while slide_num < 101
        slide_num += 1
        `rm -f tmp/zip/template/ppt/slides/slide#{slide_num.to_s}.xml`
        `rm -f tmp/zip/template/ppt/slides/_rels/slide#{slide_num.to_s}.xml.rels`
    end
      
    
    tempfile = Tempfile.new(['Worship','.pptx'])
    tempfile.close
    `rm #{tempfile.path} && cd tmp/zip/template && zip -r #{tempfile.path} .`
    tempfile.open
#    tempfile.rewind
    @worship_slide.data = tempfile.read
    @worship_slide.save!
    tempfile.unlink
    

#    slide_num += extract_slide(@worship_slide,destination,slide_num)
#    slide_num += extract_slide(@worship_slide,destination,slide_num)
    
    
    
  end

  def destroy_data
      @worship_slide.data = nil
      @worship_slide.save
      respond_to do |format|
        format.html { redirect_to @worship_slide, notice: 'Worship slide data was successfully destroyed.'}
        format.json { render :show, status: :ok, location: @worship_slide }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_worship_slide
      @worship_slide = WorshipSlide.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def worship_slide_params
      params.require(:worship_slide).permit(:date, :song1_id, :song2_id, :song3_id, :song4_id, :song5_id, :song6_id, :hymn1_id, :verse_id, :hymn2_id, :hymn3_id)
    end
    
end
