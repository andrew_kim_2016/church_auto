class WorshipSlidesController < ApplicationController
  before_action :set_worship_slide, only: [:show, :edit, :update, :destroy]

  # GET /worship_slides
  # GET /worship_slides.json
  def index
    @worship_slides = WorshipSlide.order(date: :desc).paginate(page: params[:page], per_page: 10)
  end

  # GET /worship_slides/1
  # GET /worship_slides/1.json
  def show
  end

  # GET /worship_slides/new
  def new
    @worship_slide = WorshipSlide.new
    @worship_slide.date = Date.today
  end

  # GET /worship_slides/1/edit
  def edit
  end

  # POST /worship_slides
  # POST /worship_slides.json
  def create
    @worship_slide = WorshipSlide.new(worship_slide_params)

    respond_to do |format|
      if @worship_slide.save
        format.html { redirect_to @worship_slide, notice: 'Worship slide was successfully created.' }
        format.json { render :show, status: :created, location: @worship_slide }
      else
        format.html { render :new }
        format.json { render json: @worship_slide.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /worship_slides/1
  # PATCH/PUT /worship_slides/1.json
  def update
    @worship_slide.data = nil
    respond_to do |format|
      if @worship_slide.update(worship_slide_params)
        format.html { redirect_to @worship_slide, notice: 'Worship slide was successfully updated.' }
        format.json { render :show, status: :ok, location: @worship_slide }
      else
        format.html { render :edit }
        format.json { render json: @worship_slide.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /worship_slides/1
  # DELETE /worship_slides/1.json
  def destroy
    @worship_slide.destroy
    respond_to do |format|
      format.html { redirect_to worship_slides_url, notice: 'Worship slide was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_worship_slide
      @worship_slide = WorshipSlide.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def worship_slide_params
      params.require(:worship_slide).permit(:date, :song1_id, :song2_id, :song3_id, :song4_id, :song5_id, :song6_id, :song7_id, :hymn1_id, :verse_id, :hymn2_id, :hymn3_id, :hymn4_id)
    end
end
