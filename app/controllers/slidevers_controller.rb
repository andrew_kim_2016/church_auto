require 'rubygems'
require 'nokogiri'         
require 'open-uri'   
require 'zip'
require 'resque'
require 'active_support/dependencies'
require 'resque/job_with_status'

class SlideversController < ApplicationController

  before_action :set_slidever, only: [:show, :edit, :update, :destroy]
  $job_id = ""
  $job_id_old = '1'
  def download_file

    slidever = Slidever.find(params[:id])
    #response.status = 200
   
#    send_file path+filename, :type => 'application/pptx', :disposition => 'attachment'
   
    
#      puts "Begin\n"
#      fcreate(slidever.verslist)
#    end  
#      puts "Middle\n"
#      
     
      if slidever.data == nil and  Resque::Plugins::Status::Hash.get($job_id) == nil
        $job_id = Verseget.create(id: slidever.id)
        redirect_back(fallback_location: root_path)
       # send_file path+filename, :type => 'application/pptx', :disposition => 'attachment'
      else
        $job_id = ""
        send_data slidever.data, filename: "verses.pptx",type: 'application/pptx', disposition: 'attachment'
      end
       puts "End\n"

#    Zip::File.open(path+"Verses.zip", Zip::File::CREATE) do |zipfile|
#      input_filenames.each do |filename|
#        # Two arguments:
#        # - The name of the file as it will appear in the archive
#        # - The original file, including the path to find it
#        zipfile.add(filename, path + filename)
#      end
#    end
#   zipline(path+'Verses.pptx', path+'Verses.zip')
    #File.open("church_auto/slides/Verses.pptx", 'r') do |f|
    #  send_data f.read,  :type => "text/xml; charset=UTF-8;", :disposition =>  "attachment; filename=entries.xml"
    #end
#   send_data , :filename => "verse.pptx", :type => "application/pptx"
    
   # redirect_back(fallback_location: root_path)
    
  end
  # GET /slideverst
  # GET /slidevers.json
  def index
   
    @slidevers = Slidever.order(date: :desc).paginate(page: params[:page], per_page: 10)
   
  end

  # GET /slidevers/1
  # GET /slidevers/1.json
  def show
    
  end

  # GET /slidevers/new
  def new
    @slidever = Slidever.new
    @slidever.date = Date.today
  end

  # GET /slidevers/1/edit
  def edit
  end

  # POST /slidevers
  # POST /slidevers.json
  def create
    
    
    @slidever = Slidever.new(slidever_params)

    respond_to do |format|
      if @slidever.save
        format.html { redirect_to @slidever, notice: 'Slidever was successfully created.' }
        format.json { render :show, status: :created, location: @slidever }
      else
        format.html { render :new }
        format.json { render json: @slidever.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slidevers/1
  # PATCH/PUT /slidevers/1.json
  def update
    respond_to do |format|
      if @slidever.update(slidever_params)
        format.html { redirect_to @slidever, notice: 'Slidever was successfully updated.' }
        format.json { render :show, status: :ok, location: @slidever }
      else
        format.html { render :edit }
        format.json { render json: @slidever.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slidevers/1
  # DELETE /slidevers/1.json
  def destroy
    @slidever.destroy
    respond_to do |format|
      format.html { redirect_to slidevers_url, notice: 'Slidever was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def delete_data
    set_slidever
    @slidever.data=nil
    @slidever.save
    respond_to do |format|
      format.html { redirect_to @slidever, notice: 'File data was successfully destroyed.' }
      format.json { render :show, status: :ok, location: @slidever }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slidever
      @slidever = Slidever.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slidever_params
      params.require(:slidever).permit(:date, :verslist)
    end
    
    def fcreate(listverse)
    pkg = PPTX::OPC::Package.new

    baseurl = "https://www.bibleonline.ru/search/?s="
    endurl = ""
    font_size = 40

    
    lists = listverse.split(",")
    
    lists.each do |list| 
      urlsource = baseurl+URI.encode(list," ,:")+endurl
      page = Nokogiri::HTML(open(urlsource))
      #if page.empty? then next
      #else   
      header = page.css("h2.sprite")[0].text
      #div.version-RUSV.result-text-style-normal.text-html
      slidetext = page.css("div.biblecont ol")
      if slidetext.empty? 
        then 
          header = list
          slidetext = list
          
      end
      
      #slidetext = page.css("p.verse")[0].text
      #removing leading \n
      slidetext=slidetext.to_s
      slidetext.gsub!('<ol>',"")
      slidetext.gsub!('</ol>',"")
      slidetext.gsub!('</li>',"")
      slidetext.gsub!(/[<]\w*\s\w*\W*\w*\W\s\w*\W+/,"")
      slidetext.gsub!(/\W\s\w*\W+\w\W[>]/," ")
      slidetext.gsub!('<i>'," ")
      slidetext.gsub!('</i>'," ")
      slidetext=header+" "+slidetext

      #removing RUSV string
#      slidetext = slidetext.sub "Russian Synodal Version (RUSV)", "  "
      #adding \\n
#      formatedslidetext = slidetext.gsub!(/(\B\s(\d+))/,'\n\1')
      #replacing \\n with \n
#      formatedslidetext.gsub!("\\n","\n")
      
      slide = PPTX::Slide.new(pkg)
      
      if slidetext.length < 200 then
      font_size = 48
      elsif slidetext.length < 373 then
      font_size = 40
      elsif slidetext.length <766 then
      font_size = 28
      else
        font_size = 18
      end 
      
        
      #slide.add_textbox PPTX::cm(2, 1, 22, 2), 'Title :)', sz: 45*PPTX::POINT
      slide.add_textbox PPTX::cm(0, 0, 28, 21), slidetext, sz: font_size*PPTX::POINT

      pkg.presentation.add_slide(slide)
    end 
     #puts "Created file: "+'Verses '+Time.now.utc.to_s+'.pptx'
    path = 'tmp/'
    File.open(path+'Verses.pptx', 'wb') {|f| f.write(pkg.to_zip) }
#      zipfile.get_output_stream("Verses.pptx") {|f| f.write(pkg.to_zip) }
   #send_data  pkg.to_zip,  :type => "application/pptx; charset=UTF-8;", :disposition =>  "attachment; filename=vrs.pptx"
    #end 
    end
    
    def fcreate_backup(listverse)
    pkg = PPTX::OPC::Package.new
    baseurl = "https://www.biblegateway.com/passage/?search="
    endurl = "&version=RUSV"
    font_size = 40
    
    lists = listverse.split(",")
    
     lists.each do |list| 
      urlsource = baseurl+URI.encode(list," ,:")+endurl
      page = Nokogiri::HTML(open(urlsource))   
     
      slidetext = page.css("div.version-RUSV.result-text-style-normal.text-html")[0].text
   
      #removing leading \n
      slidetext.sub!"\n",""
      #removing RUSV string
      slidetext = slidetext.sub "Russian Synodal Version (RUSV)", "  "
      #adding \\n
      formatedslidetext = slidetext.gsub!(/(\B\s(\d+))/,'\n\1')
      #replacing \\n with \n
      formatedslidetext.gsub!("\\n","\n")
      
      slide = PPTX::Slide.new(pkg)
      
      if slidetext.length < 373 then
      font_size = 40
      elsif slidetext.length <766 then
      font_size = 28
      else
        font_size = 18
      end 
      #slide.add_textbox PPTX::cm(2, 1, 22, 2), 'Title :)', sz: 45*PPTX::POINT
      slide.add_textbox PPTX::cm(0, 0, 25.5, 26), formatedslidetext, sz: font_size*PPTX::POINT
      pkg.presentation.add_slide(slide)
    end 
     #puts "Created file: "+'Verses '+Time.now.utc.to_s+'.pptx'
    path = 'tmp/'
    File.open(path+'Verses.pptx', 'wb') {|f| f.write(pkg.to_zip) }
#      zipfile.get_output_stream("Verses.pptx") {|f| f.write(pkg.to_zip) }
   
    
    end
   
end
