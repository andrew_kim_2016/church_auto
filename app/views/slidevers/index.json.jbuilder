json.array!(@slidevers) do |slidever|
  json.extract! slidever, :id, :date, :verslist
  json.url slidever_url(slidever, format: :json)
end
