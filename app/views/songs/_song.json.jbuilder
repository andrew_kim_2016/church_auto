json.extract! song, :id, :date, :name, :data, :created_at, :updated_at
json.url song_url(song, format: :json)