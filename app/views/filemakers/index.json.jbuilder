json.array!(@filemakers) do |filemaker|
  json.extract! filemaker, :id, :listverse, :slidever_id
  json.url filemaker_url(filemaker, format: :json)
end
