class WorshipSlide < ApplicationRecord
    belongs_to :song1, :class_name => "Song"
    belongs_to :song2, :class_name => "Song"
    belongs_to :song3, :class_name => "Song"
    belongs_to :song4, :class_name => "Song"
    belongs_to :song5, :class_name => "Song"
    belongs_to :song6, :class_name => "Song"
    belongs_to :song7, :class_name => "Song"
    belongs_to :hymn1, :class_name => "Song"
    belongs_to :hymn2, :class_name => "Song"
    belongs_to :hymn3, :class_name => "Song"
    belongs_to :hymn4, :class_name => "Song"
    belongs_to :verse, :class_name => "Slidever"
end
