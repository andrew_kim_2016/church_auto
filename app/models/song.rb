class Song < ApplicationRecord
   validates_uniqueness_of :name
   
   has_one :song1 ,  :class_name => 'Song', :foreign_key => 'song1'
   has_one :song2 ,  :class_name => 'Song', :foreign_key => 'song2'
   has_one :song3 ,  :class_name => 'Song', :foreign_key => 'song3'
   has_one :song4 ,  :class_name => 'Song', :foreign_key => 'song4'
   has_one :song5 ,  :class_name => 'Song', :foreign_key => 'song5'
   has_one :song6 ,  :class_name => 'Song', :foreign_key => 'song6'
   has_one :song7 ,  :class_name => 'Song', :foreign_key => 'song6'
   has_one :hymn1 ,  :class_name => 'Song', :foreign_key => 'hymn1'
   has_one :hymn2 ,  :class_name => 'Song', :foreign_key => 'hymn2'
   has_one :hymn3 ,  :class_name => 'Song', :foreign_key => 'hymn3'
   has_one :hymn4 ,  :class_name => 'Song', :foreign_key => 'hymn3'
end
