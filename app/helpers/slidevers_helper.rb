module SlideversHelper
    def get_job_status(slide)
       
        if slide.data != nil
            @status = 'File created'
        else    if $job_id !=""
                    @status =  Resque::Plugins::Status::Hash.get($job_id) 
                else
                    @status = "Need to create"
                end
        end
    end 
end
