# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170312022300) do

  create_table "biblebooklists", force: :cascade do |t|
    t.string   "name"
    t.string   "translation"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "filemakers", force: :cascade do |t|
    t.text     "listverse"
    t.integer  "slidever_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["slidever_id"], name: "index_filemakers_on_slidever_id"
  end

  create_table "slidevers", force: :cascade do |t|
    t.date     "date"
    t.text     "verslist"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.binary   "data"
  end

  create_table "songs", force: :cascade do |t|
    t.date     "date"
    t.string   "name"
    t.binary   "data"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "typehymn",   default: false
    t.index ["name"], name: "index_songs_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.boolean  "admin"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  create_table "worship_slides", force: :cascade do |t|
    t.date     "date"
    t.integer  "song1_id"
    t.integer  "song2_id"
    t.integer  "song3_id"
    t.integer  "song4_id"
    t.integer  "song5_id"
    t.integer  "song6_id"
    t.integer  "hymn1_id"
    t.integer  "verse_id"
    t.integer  "hymn2_id"
    t.integer  "hymn3_id"
    t.binary   "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "song7_id"
    t.integer  "hymn4_id"
    t.index ["hymn1_id"], name: "index_worship_slides_on_hymn1_id"
    t.index ["hymn2_id"], name: "index_worship_slides_on_hymn2_id"
    t.index ["hymn3_id"], name: "index_worship_slides_on_hymn3_id"
    t.index ["hymn4_id"], name: "index_worship_slides_on_hymn4_id"
    t.index ["song1_id"], name: "index_worship_slides_on_song1_id"
    t.index ["song2_id"], name: "index_worship_slides_on_song2_id"
    t.index ["song3_id"], name: "index_worship_slides_on_song3_id"
    t.index ["song4_id"], name: "index_worship_slides_on_song4_id"
    t.index ["song5_id"], name: "index_worship_slides_on_song5_id"
    t.index ["song6_id"], name: "index_worship_slides_on_song6_id"
    t.index ["song7_id"], name: "index_worship_slides_on_song7_id"
    t.index ["verse_id"], name: "index_worship_slides_on_verse_id"
  end

end
