class AddTypehymnToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :typehymn, :boolean, :default => false
  end
end
