class CreateWorshipSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :worship_slides do |t|
      t.date :date
      t.references :song1
      t.references :song2
      t.references :song3
      t.references :song4
      t.references :song5
      t.references :song6
      t.references :hymn1
      t.references :verse
      t.references :hymn2
      t.references :hymn3
      t.binary     :data
      
      t.timestamps
    end
    

  end
end
