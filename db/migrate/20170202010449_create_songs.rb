class CreateSongs < ActiveRecord::Migration[5.0]
  def change
    create_table :songs do |t|
      t.date :date
      t.string :name 
      t.binary :data

      t.timestamps
    end
  end
end
