class CreateBiblebooklists < ActiveRecord::Migration[5.0]
  def change
    create_table :biblebooklists do |t|
      t.string :name
      t.string :translation

      t.timestamps
    end
  end
end
