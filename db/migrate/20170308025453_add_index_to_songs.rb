class AddIndexToSongs < ActiveRecord::Migration[5.0]
  def change
   # add_column :songs, :name, :string
    add_index :songs, :name, unique: true
  end
end
