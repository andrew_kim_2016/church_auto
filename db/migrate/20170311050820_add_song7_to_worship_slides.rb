class AddSong7ToWorshipSlides < ActiveRecord::Migration[5.0]
  def change
    add_reference :worship_slides, :song7, references: :songs
   # add_foreign_key :worship_slides, :songs, column: :song7
  end
end
