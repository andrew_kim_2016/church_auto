class CreateSlidevers < ActiveRecord::Migration
  def change
    create_table :slidevers do |t|
      t.date :date
      t.text :verslist

      t.timestamps null: false
    end
  end
end
