class CreateFilemakers < ActiveRecord::Migration
  def change
    create_table :filemakers do |t|
      t.text :listverse
      t.references :slidever, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
