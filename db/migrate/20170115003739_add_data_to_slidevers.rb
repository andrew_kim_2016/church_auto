class AddDataToSlidevers < ActiveRecord::Migration[5.0]
  def change
    add_column :slidevers, :data, :binary
  end
end
