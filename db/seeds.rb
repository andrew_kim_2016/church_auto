# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Slidever.create!([{date: '2000-10-02',
                  verslist: 'Romans 3:1-3, John 3:16, Revelations 1:1-5, Genesis 1:1-10'}])
WorshipSlide.create!([{ date: "2017-02-15", 
                        song1_id: 2, 
                        song2_id: 1, 
                        song3_id: nil, 
                        song4_id: nil, 
                        song5_id: nil, 
                        song6_id: nil, 
                        hymn1_id: 2, 
                        verse_id: 1, 
                        hymn2_id: 2, 
                        hymn3_id: nil, 
                         }])
                  
User.create!(name:  "Default User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Default Admin",
             email: "example2@railstutorial.org",
             password:              "admin2",
             password_confirmation: "admin2",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

#5.times do |n|
#  name  = Faker::Name.name
#  email = "example-#{n+1}@railstutorial.org"
#  password = "password"
#  User.create!(name:  name,
#              email: email,
#              password:              password,
#              password_confirmation: password,
#              activated: true,
#              activated_at: Time.zone.now)
#end